OMS C++ example reading with curl and parsing json with boost::property_tree.

Linux build requirements (CC7 equivalents):
```
cmake gcc-c++ curl-devel boost-devel
```

Runtime requirements (CC7):
```
curl boost
```

Building:
```
cmake .
make
```

Run:
```
./omsClientDemo
```
