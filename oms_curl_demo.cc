#include <curl/curl.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <iostream>

static size_t getBodyCallback(void* contents, size_t size, size_t nmemb, void* ptr) {
  // Cast ptr to std::string pointer and append contents to that string
  ((std::string*)ptr)->append((char*)contents, size * nmemb);
  return size * nmemb;
}

unsigned long httpGet(const std::string& urlString, std::string& info) {
  CURL* curl;
  CURLcode res;
  std::string body;
  char errbuf[CURL_ERROR_SIZE];

  curl = curl_easy_init();
  unsigned long ret = false;
  if (curl) {
    struct curl_slist* chunk = nullptr;
    chunk = curl_slist_append(chunk, "content-type:application/json");
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
    curl_easy_setopt(curl, CURLOPT_URL, urlString.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, getBodyCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &body);
    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errbuf);
    res = curl_easy_perform(curl);
    curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &ret);
    if (CURLE_OK == res) {
      info = body;
    } else {
      size_t len = strlen(errbuf);
      fprintf(stderr, "\nlibcurl: (%d) ", res);
      if (len)
        fprintf(stderr, "%s%s", errbuf, ((errbuf[len - 1] != '\n') ? "\n" : ""));
      else
        fprintf(stderr, "%s\n", curl_easy_strerror(res));
    }
    curl_easy_cleanup(curl);
  }
  return ret;
}


int main() {

  std::string url = "http://vocms0184.cern.ch/agg/api/v1/streams?fields=stream_name,rate&page[offset]=0&page[limit]=1&filter[run_number][EQ]=347302";
  std::string out;

  unsigned long status = httpGet(url, out);

  if (status!=200 && status!=201) {
      std::cout << "return code " << status;
      exit(2);
  }

  namespace pt = boost::property_tree;

  pt::ptree root;

  //std::cout << " string reply: " << out << std::endl;

  std::stringstream ss;
  ss << out;
  try {
    pt::read_json(ss, root);
  } catch (pt::json_parser_error const&ex) {
      std::cout << "json_parser_error exception" << std::endl;
      exit(3);
  }
 
  std::vector< std::pair<std::string, std::string> > attributes;
  
  for (pt::ptree::value_type &data : root.get_child("data")) {
    //std::string name = attribute.first;
    //std::cout << "data name " << name << std::endl;

    auto id = data.second.get<std::string>("id");
    auto type = data.second.get<std::string>("type");
    std::cout << "id: " << id << std::endl;
    std::cout << "type: " << type << std::endl;

    for (pt::ptree::value_type &attribute : data.second.get_child("attributes")) {
      std::string name = attribute.first;
      std::string value = attribute.second.data();
      attributes.push_back(std::make_pair(name, value));
      std::cout << "attribute - " << name << ": " << value << std::endl;
    }
    std::cout << std::endl;
    auto & attributes = data.second.get_child("attributes");
    std::cout << "rate (double): " << attributes.get<double>("rate") << std::endl << std::endl;

    try {
      std::cout << "stream name (string): " << attributes.get<std::string>("streamname") << std::endl;
    } catch (pt::ptree_bad_path const&e) {
        std::cout << "bad path exception" << std::endl;
    }

    try {
      std::cout << "stream name (double): " << attributes.get<unsigned int>("stream_name") << std::endl;
    } catch (pt::ptree_bad_data const&e) {
        std::cout << "bad data exception" << std::endl;
    }

  }
}

